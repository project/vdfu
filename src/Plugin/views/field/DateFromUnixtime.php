<?php

namespace Drupal\vdfu\Plugin\views\field;

use Drupal\Core\Form\FormStateInterface;
use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\Plugin\views\field\Date;
use Drupal\Core\Datetime\Entity\DateFormat;
use Drupal\views\ResultRow;

/**
 * A handler to query date fields using FROM_UNIXTIME SQL function.
 *
 * @ingroup views_field_handlers
 *
 * @ViewsField("date_from_unixtime")
 */
class DateFromUnixtime extends Date {

  /**
   * {@inheritdoc}
   */
  public function usesGroupBy() {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function query($use_groupby = FALSE) {
    // Use parent Date field handler if from_unixtime is not enabled.
    if (empty($this->options['from_unixtime'])) {
      return parent::query($use_groupby);
    }

    $this->ensureMyTable();
    $params = $this->options['group_type'] != 'group' ? ['function' => $this->options['group_type']] : [];
    $pattern = $this->getDatePattern();

    $formula = $this->query->getDateFormat("FROM_UNIXTIME($this->tableAlias.$this->realField)", $pattern);
    $this->field_alias = $this->query->addField('', $formula, $this->tableAlias . '_' . $this->field, $params);

    $this->addAdditionalFields();
  }

  /**
   * Get the date pattern defined in view field settings.
   *
   * @return string
   *   The date format pattern.
   */
  protected function getDatePattern() {
    if (empty($this->options['date_format'])) {
      return '';
    }

    $this->format = $this->options['date_format'];
    if ($this->format === 'custom') {
      return !empty($this->options['custom_date_format'])
        ? $this->options['custom_date_format']
        : '';
    }
    else {
      /* @var DateFormat $formatter */
      $formatter = DateFormat::load($this->format);
      return $this->format_string = empty($formatter) ? '' : $formatter->getPattern();
    }
  }

  /**
   * {@inheritdoc}
   */
  public function render(ResultRow $values) {
    if ($this->options['from_unixtime']) {
      return FieldPluginBase::render($values);
    }
    return parent::render($values);
  }

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();

    $options['from_unixtime'] = ['default' => FALSE];
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);

    $form['from_unixtime'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Use FROM_UNIXTIME SQL to query date'),
      '#description' => $this->t('Use the SQL databse to format the date. This enables date values to be used in grouping aggregation.'),
      '#default_value' => $this->options['from_unixtime'],
    ];
  }

}
