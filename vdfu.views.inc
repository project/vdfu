<?php

/**
 * @file
 * Provide views data the vdfu module.
 */

/**
 * Implements hook_views_data_alter().
 */
function vdfu_views_data_alter(&$data) {
  // Loop through views data to find date fields and apply the new handler.
  foreach ($data as &$table) {
    foreach ($table as &$field) {
      if (!empty($field['field']) && !empty($field['field']['id']) && $field['field']['id'] == 'date') {
        $field['field']['id'] = 'date_from_unixtime';
      }
    }
  }
}
