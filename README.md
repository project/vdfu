About
=====
Date fields in views are formatted on the render side, which makes it hard to aggregate content without having duplicates.

This module provides the option to format the date at query time using the FROM_UNIXTIME SQL function, adding the ability to group results by the formatted date.

The **views field handler** this module provides extends the original date field handler, so if you do not enable this feature in the **views field configuration** it will defualt to the original views field handler.

Usage
=====

* Enable this module
* Add or edit a date field in your view.
* In the "Configure Field" pop-up, enable the "Use FROM_UNIXTIME SQL to query date" option
* Set the date format you want to use.
